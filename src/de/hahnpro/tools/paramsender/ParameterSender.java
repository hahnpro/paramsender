package de.hahnpro.tools.paramsender;

import de.bsvrz.dav.daf.main.*;
import de.bsvrz.dav.daf.main.config.*;
import de.bsvrz.pat.sysbed.main.JsonSerializer;
import de.bsvrz.sys.funclib.application.StandardApplication;
import de.bsvrz.sys.funclib.application.StandardApplicationRunner;
import de.bsvrz.sys.funclib.commandLineArgs.ArgumentList;
import de.kappich.sys.funclib.json.Json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class ParameterSender implements StandardApplication {

    private ClientDavInterface connection;
    private SystemObject systemObject;
    private Data data;
    private DataDescription dataDescription;
    private DataSender dataSender;
    private String sourceFilePath;

    public static void main(String[] args) {
        StandardApplicationRunner.run(new ParameterSender(), args);
    }

    private ParameterSender() {}

    @Override
    public void parseArguments(ArgumentList argumentList) throws Exception {
        sourceFilePath = argumentList.fetchArgument("-quelle=").asString();
    }

    @Override
    public void initialize(ClientDavInterface connection) throws Exception {
        this.connection = connection;
        this.dataSender = new DataSender();
        readAndSendParams();
        connection.disconnect(false, null);
    }

    private void readAndSendParams() {
        File file = new File(sourceFilePath);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("ISO-8859-1")))) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {

                String[] params = line.split(";",4);
                if (params.length != 4) {
                    continue;
                }

                getData(params);
                subscribe();
                sendData();
                unsubscribe();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getData(String[] params) {

        DataModel dataModel = connection.getDataModel();
        systemObject = dataModel.getObject(params[0]);

        AttributeGroup atg = dataModel.getAttributeGroup(params[1]);
        Aspect aspect = dataModel.getAspect(params[2].isEmpty() ? "asp.parameterVorgabe" : params[2]);
        dataDescription = new DataDescription(atg, aspect);

        try {
            Data tmp = connection.createData(atg);
            data = tmp.createModifiableCopy();
            Object json = Json.getInstance().readObject(params[3]);
            JsonSerializer.deserializeData(json, data);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void subscribe() {
        try {
            connection.subscribeSender(dataSender, systemObject, dataDescription, SenderRole.sender());
        }
        catch (OneSubscriptionPerSendData e) {
            throw new RuntimeException(e);
        }
    }

    private void unsubscribe() {
        connection.unsubscribeSender(dataSender, systemObject, dataDescription);
    }

    private void sendData() {
        ResultData resultData = new ResultData(systemObject, dataDescription, System.currentTimeMillis(), data);
        try {
            connection.sendData(resultData);
        }
        catch (SendSubscriptionNotConfirmed e) {
            e.printStackTrace();
        }
    }

    private static class DataSender implements ClientSenderInterface {

        @Override
        public void dataRequest(SystemObject systemObject, DataDescription dataDescription, byte state) {}

        @Override
        public boolean isRequestSupported(SystemObject systemObject, DataDescription dataDescription) {
            return true;
        }

    }

}
